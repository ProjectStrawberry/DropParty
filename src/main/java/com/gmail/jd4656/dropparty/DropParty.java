package com.gmail.jd4656.dropparty;

import co.aikar.commands.PaperCommandManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class DropParty extends JavaPlugin {

    public DropParty plugin;
    FileConfiguration config;

    boolean status = false;
    List<ItemStack> items = new ArrayList<>();

    @Override
    public void onEnable() {
        plugin = this;

        this.config = plugin.getConfig();

        PaperCommandManager commandManager = new PaperCommandManager(this);
        commandManager.enableUnstableAPI("help");
        commandManager.registerCommand(new Commands(this));
    }
}
