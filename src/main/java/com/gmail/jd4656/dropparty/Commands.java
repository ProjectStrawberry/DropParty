package com.gmail.jd4656.dropparty;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.Random;

@CommandAlias("dropparty|dp")
@CommandPermission("dropparty.use")
public class Commands extends BaseCommand {

    DropParty plugin;
    Random rand = new Random();
    BukkitTask task;

    Commands(DropParty p) {
        plugin = p;
    }

    @Subcommand("setlocation")
    @Description("Sets the location and radius of the drop party.")
    @CommandCompletion("<radius>")
    public void setLocation(Player player, int radius) {
        if (radius < 1) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The radius must be at least 1.");
            return;
        }

        plugin.config.set("centerLocation", player.getLocation());
        plugin.config.set("radius", radius);
        plugin.saveConfig();

        player.sendMessage(ChatColor.GREEN + "You've set the drop area.");
    }

    @Subcommand("status")
    @Description("Displays the status of the drop party")
    public void status(CommandSender sender) {
        int itemCount = 0;
        for (ItemStack item : plugin.items) {
            itemCount += item.getAmount();
        }

        sender.sendMessage(ChatColor.YELLOW + "Status: " + (plugin.status ? ChatColor.GREEN + "Dropping Items" : ChatColor.RED + "Waiting"));
        sender.sendMessage(ChatColor.YELLOW + "Items Remaining: " + ChatColor.RED + itemCount);
        sender.sendMessage(ChatColor.YELLOW + "Drop Rate: " + ChatColor.RED + (plugin.config.contains("dropRate") ? plugin.config.getInt("dropRate") : "Not Set") +
            ChatColor.YELLOW + " Radius: " + ChatColor.RED + (plugin.config.contains("radius") ? plugin.config.getInt("radius") : "Not Set"));
        sender.sendMessage(ChatColor.YELLOW + "ETA: " + ChatColor.RED + (plugin.config.contains("dropRate") ? convertTime((itemCount / plugin.config.getInt("dropRate")) * 1000) : "No drop rate set"));
    }

    @Subcommand("setrate")
    @Description("Sets how many items will drop per second.")
    @CommandCompletion("<items-per-second>")
    public void setrate(CommandSender player, int dropRate) {
        if (dropRate < 1) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The drop rate must be at least 1.");
            return;
        }

        plugin.config.set("dropRate", dropRate);
        plugin.saveConfig();

        player.sendMessage(ChatColor.GREEN + "You've set the item drop rate.");
    }

    @Subcommand("stop")
    @Description("Stops a drop party")
    public void stop(CommandSender player) {
        if (!plugin.status) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no drop party currently running.");
            return;
        }

        task.cancel();
        plugin.status = false;
        Bukkit.broadcastMessage(ChatColor.RED + "[" + ChatColor.DARK_RED + "DropParty" + ChatColor.RED + "] " + ChatColor.YELLOW + "The drop party has been stopped!");
    }

    @Subcommand("start")
    @Description("Starts dropping items")
    @CommandCompletion("<tier-number>")
    public void start(CommandSender player, String tierName) {
        if (!plugin.config.contains("centerLocation") || !plugin.config.contains("radius")) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You need to set a location with " + ChatColor.RED + "/dropparty setlocation <radius>");
            return;
        }

        if (!plugin.config.contains("dropRate")) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You need to set a location with " + ChatColor.RED + "/dropparty setlocation <radius>");
            return;
        }

        if (plugin.items.isEmpty()) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You need to add items with " + ChatColor.RED + "/dropparty addchest");
            return;
        }

        if (plugin.status) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The drop party is currently running.");
            return;
        }

        plugin.status = true;
        Location centerLocation = plugin.config.getLocation("centerLocation");

        task = new BukkitRunnable() {
            @Override
            public void run() {
                for (int i=0; i<plugin.config.getInt("dropRate"); i++) {
                    if (!dropItem(centerLocation, tierName)) break;
                }
            }
        }.runTaskTimer(plugin, 0, 20L);

        Bukkit.broadcastMessage(ChatColor.DARK_RED + "[" + ChatColor.RED + "DropParty" + ChatColor.DARK_RED + "] " + ChatColor.YELLOW + "Tier " + tierName + " is now dropping items!");
    }

    @Subcommand("clear")
    @Description("Clears all items added to the drop party")
    public void clear(CommandSender player) {
        if (plugin.status) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't remove items while a drop party is in progress.");
            return;
        }

        plugin.items.clear();
        player.sendMessage(ChatColor.GREEN + "The items in this drop party have been reset.");
    }

    @Subcommand("addchest")
    @Description("Adds the chest you are looking at to the drop party.")
    public void addchest(Player player) {
        if (plugin.status) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't add items while a drop party is in progress.");
            return;
        }

        Block block = player.getTargetBlockExact(5);
        if (block == null || block.getType() != Material.CHEST) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You must be looking at a chest for this command to work.");
            return;
        }

        int addedCount = 0;
        int totalItems = 0;

        Container container = (Container) block.getState();
        for (ItemStack item : container.getInventory().getContents()) {
            if (item == null || item.getType() == Material.AIR) continue;
            addedCount += item.getAmount();
            plugin.items.add(item.clone());
        }

        for (ItemStack item : plugin.items) totalItems += item.getAmount();

        player.sendMessage(ChatColor.GREEN + "Added " + ChatColor.RED + addedCount + ChatColor.GREEN + " items to the drop party. Total added: " + ChatColor.RED + totalItems);
    }

    @Default
    @HelpCommand
    public void doHelp(CommandHelp help) {
        help.showHelp();
    }

    boolean dropItem(Location centerLocation, String tierName) {
        if (plugin.items.isEmpty()) {
            task.cancel();
            Bukkit.broadcastMessage(ChatColor.DARK_RED + "[" + ChatColor.RED + "DropParty" + ChatColor.DARK_RED + "] " + ChatColor.YELLOW + "Tier " + tierName + " has finished dropping items!");
            plugin.status = false;
            return false;
        }

        int index = randNum(0, plugin.items.size() - 1);
        ItemStack item = plugin.items.get(index);
        ItemStack itemToDrop = item.clone();
        itemToDrop.setAmount(1);

        item.setAmount(item.getAmount() - 1);
        if (item.getAmount() < 1) {
            plugin.items.remove(index);
        }


        if (centerLocation.getWorld() == null) {
            plugin.getLogger().warning("Could not find world to drop items.");
            task.cancel();
            Bukkit.broadcastMessage(ChatColor.DARK_RED + "[" + ChatColor.RED + "DropParty" + ChatColor.DARK_RED + "] " + ChatColor.YELLOW + "Tier " + tierName + " has stopped dropping items.");
            plugin.status = false;
            return false;
        }

        centerLocation.getWorld().dropItem(getRandomLocation(centerLocation), itemToDrop);
        return true;
    }

    int randNum(int min, int max) {
        return rand.nextInt((max - min) + 1) + min;
    }

    Location getRandomLocation(Location centerLoc) {
        double centerX = centerLoc.getX();
        double centerZ = centerLoc.getZ();

        double angle = rand.nextDouble() * 360;
        double radians = Math.toRadians(angle);
        int x = (int) (Math.round(centerX + (rand.nextDouble() * plugin.config.getInt("radius") * Math.cos(radians))) + 0.5);
        int z = (int) (Math.round(centerZ + (rand.nextDouble() * plugin.config.getInt("radius") * Math.sin(radians))) + 0.5);

        Location location = centerLoc.clone();
        location.setX(x);
        location.setZ(z);
        location.setY(location.getY() + randNum(1, 10));

        return location;
    }

    String convertTime(long ms) {
        if (ms < 1000) {
            return "less than one second";
        }

        long seconds = (ms / 1000) % 60;
        long minutes = (ms / (1000 * 60)) % 60;
        long hours = (ms / (1000 * 60 * 60)) % 24;
        long days = (ms / (1000 * 60 * 60 * 24));

        String reply = "";
        if (days > 0) {
            reply += days + (days == 1 ? " day " : " days ");
        }
        if (hours > 0) {
            reply += hours + (hours == 1 ? " hour " : " hours ");
        }
        if (minutes > 0) {
            reply += minutes + (minutes == 1 ? " minute " : " minutes ");
        }
        if (seconds > 0 && hours < 1) {
            reply += seconds + (seconds == 1 ? " second " : " seconds ");
        }

        return reply.trim();
    }
}
